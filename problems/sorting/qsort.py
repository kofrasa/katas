#!/usr/bin/env python

def qsort(arr):
    if not arr:
        return []
    pivots = [x for x in arr if x == arr[0]]
    less = [s for s in arr if s < arr[0]]
    greater = [s for s in arr if s > arr[0]]
    return qsort(less) + pivots + qsort(greater)


def main():
    arr = [random.randint(1, 30) for i in range(5)] + [2, 4, 2]
    print("arr: %s" % arr)
    print("qsort: %s" % qsort(arr))


if __name__ == '__main__':
    main()
