#!/usr/bin/env python


def merge(left, right):
    """Merges 2 sorted arrays
    """
    if left is None or right is None:
        raise Exception("bad input")
    i, j = 0, 0
    tmp = []
    while i < len(left) and j < len(right):
        if left[i] <= right[j]:
            tmp.append(left[i])
            i += 1
        else:
            tmp.append(right[j])
            j += 1
    if i < len(left):
        tmp.extend(left[i:])
    if j < len(right):
        tmp.extend(right[j:])

    return tmp

def merge_sort(arr):
    if arr is None:
        raise Exception("bad input")
    if len(arr) == 1:
        return arr
    mid = len(arr) / 2
    left = merge_sort(arr[:mid])
    right = merge_sort(arr[mid:])
    return merge(left, right)


def main():
    xs = [random.randint(0, 20) for i in range(20)]
    print("array: \n%s" % xs)
    rs = merge_sort(xs)
    assert len(xs) == len(rs)
    print("merge sort: \n%s" % rs)


if __name__ == '__main__':
    main()
