#!/usr/bin/env python

def keyindexed_countsort(arr):
    # Goal: Sort an array A of N integers between 0 and R-1
    # Solution:
    # 1. count frequencies of each letter using key as index
    # 2. compute frequency cumulates which specify destinations
    # 3. access comulates using key as index to move items
    # 4. copy back into original
    N = len(arr)
    R = max(arr) #O(n)
    count = [0] * (R+1)
    aux = [0] * N
    for i in range(1, N):
        count[arr[i]] += 1
    for r in range(R):
        count[r+1] += count[r]
    for i in range(N):
        k = count[arr[i]]
        count[arr[i]] += 1
        aux[i] = k
    for i in range(N):
        arr[i] = aux[i]

def main():
    print(keyindexed_countsort([9,4,6,2,6,8,5,2,1,4,2,2,1,8,6,7]))

if __name__ == '__main__':
    main()
