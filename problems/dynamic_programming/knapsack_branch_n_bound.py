#!/usr/bin/env python

def knapsack_branch_n_bound(capacity, weights, values):
    # compute densities
    densities = [(values[i]/weights[i], i) for i in range(len(values))]
    # sort highest to lowest
    densities.sort(key=lambda x: x[0], reverse=True)
    best, cap, i = 0, capacity, 0
    # compute the optimistic best value
    while cap > 0:
        j = densities[i][1]
        v, w = values[j], weights[j]
        if cap - w >= 0:
            cap -= w
            best += v
        else:
            ratio = 1 - (w - cap)/w
            cap = 0
            best += v * ratio
        i += 1
    # perform branch and bound
    #TODO
