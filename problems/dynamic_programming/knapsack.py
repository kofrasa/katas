#!/usr/bin/env python

def knapsack(capacity, weights, values):
    # Objective: MAX( O(k,j-1), v[j] + O(k-w[j], j-1) )
    # Constraint: SUM(w[j]*x[j]) <= k
    # k: value of knapsack
    # w[j]: weight of item (j)
    # x[j]: decision variable of item (j) where x[j] in {0,1} for not selected or selected.
    # v[j]: value of item (j)
    memo = {}
    def solve(k, j):
        # k = remaining_weight
        # j = index of item
        if j < 0 or k == 0:
            return 0
        else:
            key = (k,j)
            if key in memo:
                return memo[key]
            if weights[j] <= k:
                res = max([ solve(k, j-1), values[j] + solve(k-weights[j], j-1) ])
            else:
                res = solve(k, j-1)
        memo[key] = res
        return res
    return solve(capacity, len(values)-1)

def main():
    capacity = 8
    weights = [2, 2, 4, 5]
    values = [2, 4, 6, 9]
    expected = 13
    result = knapsack(8, weights, values)
    print("capacity:", capacity, "\nweights:", weights, "\nvalues:", values, "\nexpected:", expected, "\nactual:", result)
    assert expected == result

if __name__ == '__main__':
    main()
