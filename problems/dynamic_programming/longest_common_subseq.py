#!/usr/bin/env python


def find_lcs(X,Y): #List[String]
    m, n = len(X), len(Y)

def lcs_iter(X, Y):
    """Bottom-up approach using iteration and tabulation
    """
    m, n = len(X), len(Y)
    L = [[0]*(n+1) for _ in range(m+1)]

    for i in range(m+1):
        for j in range(n+1):
            if i == 0 or j == 0:
                L[i][j] = 0
            elif X[i-1] == Y[j-1]:
                L[i][j] = L[i-1][j-1] + 1
            else:
                L[i][j] = max(L[i-1][j], L[i][j-1])
    return L[m][n]

# compute the length of the longest subsequence
def lcs_recur(A,B,memo=None):
    """Top-down approach using recursion and memoization
    """
    if not A or not B:
        return 0
    m, n = len(A), len(B)
    k = (m, n)
    if memo is None:
        memo = {}
    if k in memo:
        return memo[k]
    if A[m-1] == B[n-1]:
        memo[k] = lcs_recur(A[:m-1], B[:n-1], memo) + 1
        return memo[k]
    else:
        return max(lcs_recur(A[:m-1], B, memo), lcs_recur(A, B[:n-1], memo))

def longest_common_subseq(first, second):
    return lcs_iter(first, second)

def main():
    for x,y in [("abcdaf", "acbcf"), ("XMJYAUZ", "MZJAWXU"), ("ABCDEFG", "BCDGK")]:
        print("In:", (x, y), "\nOut:", longest_common_subseq(x, y), "\n")

if __name__ == '__main__':
    main()
