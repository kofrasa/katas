#!/usr/bin/env python

"""
https://leetcode.com/problems/maximum-subarray/

Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
"""


def maxSubArray(nums):
    """
    :type nums: List[int]
    :rtype: int
    """
    highest, begin, end = -float('inf'), 0, 0
    acc, index = 0, 0
    for i, n in enumerate(nums):
        acc += n
        if acc <= n:
            index = i
            acc = n
        if acc > highest:
            begin, end = index, i
            highest = acc

    return highest, begin, end

def main():
    arr = [
        [2, -1, 2, -1, 4, -5],
        [2, 8, 2, -1, 4, -5],
        [-2, 1, -3, 4, -1, 2, 1, -5, 4]
    ]
    print("returns: (max_sum, start_index, end_index)")
    for n in arr:
        print("input:", n, "result:", maxSubArray(n))

if __name__ == '__main__':
    main()
