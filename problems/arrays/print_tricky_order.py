#!/usr/bin/env python

def tricky_order(N, d):
    """Given a number d and size of array N. Print all combination of element in the array such that first
    element of array is d and next element in the array can be + or -1 the previous element in the array.
    """
    res = []
    hset = set()

    def perm(xs, k):
        if len(xs) == N:
            key = ''.join(map(str, xs))
            if key not in hset:
                res.append(xs)
                hset.add(key)
        else:
            for i in range(2):
                perm(xs + [k], k + 1)
                perm(xs + [k], k - 1)

    perm([], d)
    return res


def main():
    n, d = 4, 4
    print("N:", n, "d:", d, "\nOut:", tricky_order(n, d))


if __name__ == '__main__':
    main()
