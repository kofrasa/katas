#!/usr/bin/env python

def suffix_array(s):
    m = len(s)
    hash = {}
    nil = chr(0)
    hash[nil] = m + 1
    A = [None]*(m+1)
    A[0] = m+1
    for i in range(m-1,-1,-1):
        hash[s[i:]+nil] = i+1
        A[i+1] = m - i
    suffixes = list(hash.items())
    suffixes.sort(key = lambda x: x[0])
    print(A)
    return suffixes

def main():
    print(suffix_array("banana"))

if __name__ == '__main__':
    main()

