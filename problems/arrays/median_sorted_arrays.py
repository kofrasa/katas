class Solution:
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        n1 = len(nums1)
        n2 = len(nums2)
        mid = int((n1+n2)/2) + 1
        isEven = (n1+n2) % 2 == 0
        a, b = None, None
        i, j, k = 0, 0, 0
        while k < mid:
            temp = None
            if i < n1 and j < n2:
                if nums1[i] < nums2[j]:
                    temp = nums1[i]
                    i += 1
                else:
                    temp = nums2[j]
                    j += 1
            elif i < n1:
                temp = nums1[i]
                i += 1
            elif j < n2:
                temp = nums2[j]
                j += 1
            else:
                break

            if k == mid-2:
                a = temp
            if k == mid-1:
                b = temp
            k += 1

        if not isEven:
            return b
        elif b is not None:
            return (a + b) / 2
        else:
            return a

if __name__ == "__main__":
    A = [4, 20, 32, 50, 55, 61]
    B = [1, 15, 22, 30, 70]
    # 1 4 15 20 22 30 32 50 55 61 70
    print("In:", A, B, "\nOut:", Solution().findMedianSortedArrays(A, B))