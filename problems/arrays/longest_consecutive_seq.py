#!/usr/bin/env python

def longest_consecutive_seq(nums):
    """Find the longest consecutive sequence in the given array of numbers
    """
    hset = set(nums)
    result = []
    while hset:
        k = hset.pop()
        n = k - 1
        temp = []
        while n in hset:
            temp.append(n)
            hset.remove(n)
            n -= 1
        n = k + 1
        while n in hset:
            temp.append(n)
            hset.remove(n)
            n += 1
        if temp:
            # if we found something then include the value and take longest
            temp.append(k)
            if len(temp) > len(result):
                result = temp
    return result

def main():
    input = [100, 4, 200, 1, 3, 2]
    print("In:", input, "\nOut:", sorted(longest_consecutive_seq(input)))


if __name__ == '__main__':
    main()

