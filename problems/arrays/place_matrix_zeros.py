#!/usr/bin/env python

def place_matrix_zeros(mat):
    """For each cell which has the value zero, place zeros in entire column and row of the cell
    complexity: O(n^2)
    """
    if not mat:
        raise Exception("bad input")

    rows, cols = [], []
    r = len(mat)
    c = len(mat[0])
    for i in range(r):
        for j in range(c):
            if not mat[i][j]:
                rows.append(i)
                cols.append(j)
    for i in rows:
        for j in range(c):
            mat[i][j] = 0
    for j in cols:
        for i in range(r):
            mat[i][j] = 0


def main():
    mat = [[1, 1, 1, 1], [1, 1, 0, 1], [1, 1, 1, 1], [1, 1, 1, 1]]
    print("matrix: %s" % mat)
    place_matrix_zeros(mat)
    print("zeroed: %s" % mat)


if __name__ == '__main__':
    main()
