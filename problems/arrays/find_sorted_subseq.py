#!/usr/bin/env python

def find_sorted_subseq(N):
    """Find a sorted subsequence of size 3 in O(n)
    """
    n = len(N)
    s = 0
    t = n - 1
    S = [-1] * n
    T = [-1] * n
    for i in range(n):
        if N[s] < N[i]:
            S[i] = s
        else:
            s = i
    for i in range(t, -1, -1):
        if N[t] > N[i]:
            T[i] = t
        else:
            t = i
    for i in range(n):
        if S[i] != -1 and T[i] != -1:
            return (N[S[i]], N[i], N[T[i]])
    return None


def main():
    A = [12, 11, 10, 5, 6, 2, 3, 30]
    print("input: %s" % A)
    print("sorted subsequence: %s" % str(find_sorted_subseq(A)))


if __name__ == '__main__':
    main()
