#!/usr/bin/env python


def binary_search(xs, item):
    """binary search algorithm
    """
    if not xs:
        raise Exception("Invalid sequence")
    if not item:
        raise Exception("Invalid item")
    lo = 0
    hi = len(xs) - 1
    while hi >= lo:
        mid = lo + (hi - lo) / 2
        if xs[mid] < item:
            lo = mid + 1
        elif xs[mid] > item:
            hi = mid - 1
        else:
            return mid
    return -1