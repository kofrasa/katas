#!/usr/bin/env python

def count_triplets(array, k=3):
    """Given a sorted array of n integers, count and display number of triplets such that
    a[i] < a[j] < a[k] . solution is O(n)
    """
    # get unique numbers with the help of a set
    # compute "nCr" where r=k and n= size of unique numbers
    n = len(set(array))
    if n < k:
        return 0

    def fact(x):
        z = 1
        while x:
            z *= x
            x -= 1
        return z

    # compute nCk => n!/(n-k)! * k!
    return int(fact(n) / (fact(n - k) * fact(k)))


def main():
    input = [1, 2, 2, 4, 5, 5, 7, 8, 9, 9]
    print("input:", input, "\ntriplets (a[i] < a[j] < a[k]):", count_triplets(input, 3), "\n")


if __name__ == '__main__':
    main()
