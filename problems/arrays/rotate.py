#!/usr/bin/env python

def rotate(array, d):
    """rotate the array by 'd' elements
    """
    if not array:
        raise Exception("bad input")

    def reverse(lo, hi):
        while lo < hi:
            array[lo], array[hi - 1] = array[hi - 1], array[lo]
            lo += 1
            hi -= 1

    reverse(0, len(array))
    reverse(0, len(array) - d)
    reverse(len(array) - d, len(array))
    return array


def main():
    input = list(range(1, 10))
    d = 3
    print("In:", input, "\nrotate:", d, "\nOut:", rotate(input.copy(), d))


if __name__ == '__main__':
    main()
