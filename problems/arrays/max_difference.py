#!/usr/bin/env python

def max_diff(xs):
    min_index = 0
    diff = 0
    for i in range(len(xs)):
        num = xs[i]
        if num < xs[min_index]:
            min_index = i
        else:
            t = num - xs[min_index]
            diff = max(diff, t)
    return diff


def main():
    print("maximum difference is: %s" % max_diff(map(int, raw_input("enter numbers: ").split())))


if __name__ == '__main__':
    main()
