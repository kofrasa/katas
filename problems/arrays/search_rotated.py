#!/usr/bin/env python

def search_rotated(xs, item):
    if not xs:
        return -1
    if item is None:
        raise Exception("Invalid item")

    lo, hi = 0, len(xs) - 1
    while lo <= hi:
        mid = lo + (hi - lo) / 2
        if xs[mid] == item:
            return mid
        if xs[lo] <= xs[mid]:
            if item < xs[mid]:
                if item >= xs[lo]:
                    hi = mid
                else:
                    lo = mid + 1
            else:
                lo = mid + 1
        else:
            if item > xs[mid]:
                if xs[lo] > item:
                    lo = mid + 1
                else:
                    hi = mid - 1
            else:
                hi = mid - 1
    return -1


def main():
    arr = map(int, raw_input("enter values: ").split())
    n = input("enter search number: ")
    print("Index of %s is %s" % (n, search_rotated(arr, n)))


if __name__ == '__main__':
    main()
