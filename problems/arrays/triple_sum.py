#!/usr/bin/env python

def triple_sum(N, x):
    """Find three
    """
    # sort numbers in increasing order
    # for each index fix the index, and shrink window between remaining numbers
    # solution is O(n^2)
    if len(N) < 3:
        raise Exception("bad N")
    N = N[:]
    N.sort()
    for i in range(len(N) - 2):
        j = i + 1
        k = len(N) - 1
        while j < k:
            a, b, c = N[i], N[j], N[k]
            acc = sum([a, b, c])
            if acc == x:
                return (a, b, c)
            elif acc > x:
                k -= 1
            else:
                j += 1
    return None


def main():
    N = [1, 4, 45, 6, 10, 8]
    acc = 22
    print("N: %s" % N)
    print("(a<b<c) where sum=%s. expected: (4,8,10). actual: %s" % (acc, triple_sum(N, 22)))


if __name__ == '__main__':
    main()
