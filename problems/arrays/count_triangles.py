#!/usr/bin/env python

def count_triangles(N):
    """Find the number of possible triangles that can be constructed via the sequence of numbers
    """
    if not N:
        raise Exception("bad input")
    if len(N) < 3:
        raise Exception("bad input: size must be greater than 2")
    N.sort()
    n = len(N)
    count = 0
    for i in range(n):
        k = i
        for j in range(i, n):
            # Find the rightmost element which is smaller than the sum
            # of two fixed elements
            # The important thing to note here is, we use the previous
            # value of k. If value of arr[i] + arr[j-1] was greater than arr[k],
            # then arr[i] + arr[j] must be greater than k, because the
            # array is sorted.
            while k < n and N[i] + N[j] > N[k]:
                k += 1
            # Total number of possible triangles that can be formed
            # with the two fixed elements is k - j - 1.  The two fixed
            # elements are arr[i] and arr[j].  All elements between arr[j+1]
            # to arr[k-1] can form a triangle with arr[i] and arr[j].
            # One is subtracted from k because k is incremented one extra
            # in above while loop.
            # k will always be greater than j. If j becomes equal to k, then
            # above loop will increment k, because arr[k] + arr[i] is always
            # greater than arr[k]
            count += k - j
    return count


def count_triangles2(A):
    # O(n^3)
    count = 0
    n = len(A)
    for i in range(n):
        for j in range(i, n):
            for k in range(j, n):
                if A[i] + A[j] > A[k]:
                    count += 1
                    # print A[i],A[j],A[k]
    return count


def main():
    A = [10, 21, 22, 100, 101, 200, 300]
    # A = range(1,4)
    print("count triangles of %s" % A)
    print("counting triangles1: %s" % count_triangles(A))
    print("counting triangles2: %s" % count_triangles2(A))


if __name__ == '__main__':
    main()
