#!/usr/bin/env python

def is_rotation(first, second):
    """Check if one array is the rotation of another.
    """
    # 1. Find start position in second array relative to first in linear time
    #     by checking for the expected items at the start and end positions.
    # 2. Iterate from the boundaries and compare items at matching positions
    #
    # Complexity: O(n) time, O(1) space
    if first is None or second is None:
        raise Exception("one or more input is empty")
    if len(first) != len(second):
        # non-equal lengths cannot be similar
        return False
    if len(first) == 0:
        return True

    size = len(first)
    start = None
    for i in range(size):
        if second[i] == first[0]:
            j = (i + size-1) % size
            if second[j] == first[-1]:
                start = i
                break

    if start is None:
        return False
    for i in range(size):
        if first[i] != second[start]:
            return False
        start = (start + 1) % size

    return True

def main():
    fixtures = [
        [ [], [], True ],
        [ list('aa'), list('aa'), True ],
        [ list('abcdefg'), list('defgabc'), True ],
        [ list('abcdefg'), list('abcdfeg'), False ],
        [ list('a'), list('ab'), False ],
    ]

    for first, second, expected in fixtures:
        print("first:", first, "second:", second,
            "\nexpect:", expected, "\nactual:", is_rotation(first, second), "\n")

if __name__ == "__main__":
    main()
