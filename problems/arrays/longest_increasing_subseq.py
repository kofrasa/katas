#!/usr/bin/env python


def longest_increasing_subseq(xs):
    """
    Finds the longest increasing subsequence in the given sequence

    Returns all the increasing subsequences in the a
    """
    seq = []  # all increasing sequences
    indices = []  # indices of longest increasing sequences
    size = 0  # current longest size
    seen = set({}) # store seen element to reduce useless intermediate sequences
    for i in range(0, len(xs)):
        for j in range(0, len(seq)):
            if xs[i] > seq[j][-1]:
                elem = xs[i]
                t = seq[j] + [elem]
                seen.add(elem) # mark as seen
                if len(t) > size:
                    indices = [j]
                    size = len(t)
                elif len(t) == size:
                    indices.append(j)
                seq.append(t)
        if xs[i] not in seen:
            seq.append([xs[i]])
    return [seq[k] for k in indices]


def main():
    inputs = [
        [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15],
        [10, 22, 9, 33, 21, 50, 41, 60, 80]
    ]

    for xs in inputs:
        print("In:", xs, "\nOut:", longest_increasing_subseq(xs), "\n")
if __name__ == '__main__':
    main()
