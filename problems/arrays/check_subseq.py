#!/usr/bin/env python

def is_subseq(s, src):
    """Checks that s is a sub-sequence of src
    """
    if not s or not src or len(s) > len(src):
        return False

    k = len(s)
    m = len(src)
    i, j = 0, 0
    while i <= k:
        if s[j] == src[i]:
            j += 1
        i += 1
        if src[m - 1] == s[k - 1]:
            k -= 1
        m -= 1

    return not k


def main():
    s, src = raw_input("<search> <src>: ").split()
    print("is sub-sequence: %s" % is_subseq(s, src))


if __name__ == '__main__':
    main()
