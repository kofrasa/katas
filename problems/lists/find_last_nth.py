#!/usr/bin/env python

def find_nth_last(head, n):
    """Find the last nth node on the list
    """
    s = [None] * n
    i = 0
    while head:
        node = head.next
        s[i] = head
        i += 1
        i %= n
        head = node

    if s[n - 1]:
        return s[0]

    return None

def find_nth_last2(head, n):
    # O(n) time, O(1) space
    # 1. iterate to get size of list
    # 2. if n > size: n = n % size
    # 3. reverse first n elements of list
    # 4. return head