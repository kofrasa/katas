
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def reverseKGroup(self, root, k):
        # handle empty
        if not root:
            return root
        head, current, i = self.reverseK(root, k)
        # if we did not reverse k elements, restore order of last set of nodes
        if i < k:
            head, current, i = self.reverseK(head, k)
        root = head
        last = None
        # seek to last node
        while head:
            last, head = head, head.next
        if last:
            # connect last node to next group
            last.next = self.reverseKGroup(current, k)
        return root

    def reverseK(self, root, k):
        """reverse upto k nodes and return the new head, current node,
            and number of nodes reversed
        """
        if not root:
            return root
        head, current, i = root, None, 0
        while head and i < k:
            child = head.next
            head.next = current
            current = head
            head = child
            i += 1
        return current, head, i


if __name__ == "__main__":
    pass