
class Node(object):

    def __init__(self, val=None, next=None):
        self.next = next
        self.val = val

    def __repr__(self):
        return "%s->%s" % (self.val, self.next)


class DNode(object):

    def __init__(self, val=None, next=None, prev=None):
        self.val = val
        self.next = next
        self.prev = prev

    def __repr__(self):
        return str(self.val)


def append_node(head, tail, node):
    if not node or head == node or tail == node:
        return head, tail
    if tail:
        tail.next = node
        node.prev = tail
        tail = node
    elif head:
        head.next = node
        node.prev = head
    else:
        head = node
        tail = node
    print(">>head=%s,tail=%s" % (head, tail))
    return head, tail


def remove_node(head, tail, node):
    if not node or not head or not tail:
        raise Exception("One or more argument is invalid")

    print("<<head=%s,tail=%s,node=%s" % (head, tail, node))

    prev = node.prev
    next = node.next

    if prev:
        prev.next = next
    else:
        head = next

    if next:
        next.prev = prev
    else:
        tail = prev
    return head, tail


def gen_linkedlist(start=0, end=10, step=1):
    h = Node(start)
    t = h
    for i in range(start+step, end, step):
        t.next = Node(i)
        t = t.next
    return h

