#!/usr/bin/env python

from .node import Node

def mergeKLists(self, lists):
    if not lists:
        return None
    size = len(lists)
    if size == 1:
        return lists[0]
    if size == 2:
        return merge2lists(lists[0], lists[1])
    else:
        mid = int(size/2)
        left = mergeKLists(lists[0:mid+1])
        right = mergeKLists(lists[mid+1:size])
        return merge2lists(left, right)

def merge2lists(left, right):
    if not left:
        return right
    if not right:
        return left
    head, last = None, None
    while left and right:
        current = None
        if left.val < right.val:
            current = left
            left = left.next
        else:
            current = right
            right = right.next
        if not head:
            head = last = current
        else:
            last.next = current
            last = current
            last.next = None
    if left:
        last.next = left
    if right:
        last.next = right
    return head

if __name__ == "__main__":
    res = mergeKLists([
        Node(2, Node(10, Node(12, Node(21, Node(30))))),
        Node(1, Node(6, Node(15))),
        Node(11, Node(12, Node(12, Node(25, Node(35, Node(50))))))
    ])
    print(res)