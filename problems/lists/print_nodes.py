#!/usr/bin/env python

from .node import Node

def print_nodes(head, k):
    S = []
    while head:
        next = head.next
        head.next = None
        S.append(head)
        head = next
    head = tail = None
    while S:
        if len(S) % k == 0:
            for x in S[-k:]:
                if not tail:
                    head = tail = x
                else:
                    tail.next = x
                    tail = x
            del S[-k:]
        else:
            r = len(S) % k
            for x in S[-r:]:
                if not tail:
                    head = tail = x
                else:
                    tail.next = x
                    tail = x
            del S[-r:]
    return head


def main():
    head = Node('A', Node('B', Node('C', Node('D', Node('E')))))
    print("k=2, node= %s" % head)
    print("new node= %s\n" % print_nodes(head, 2))
    head = Node('A', Node('B', Node('C', Node('D', Node('E', Node('F'))))))
    print("k=2, node= %s" % head)
    print("new node= %s" % print_nodes(head, 2))


if __name__ == '__main__':
    main()
