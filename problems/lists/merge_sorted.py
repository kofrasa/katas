#!/usr/bin/env python

from .node import Node

def merge_sorted_stack(first, second):
    """Merged sorted lists
    complexity: O(n+m)
    """
    # use a stack for each list
    if first is None:
        return second
    if second is None:
        return first

    p = [first]
    q = [second]
    head, tail = None, None
    while p or q:
        n = None
        if p and q:
            if p[0].val < q[0].val:
                n = p.pop()
                if n.next:
                    p.append(n.next)
            else:
                n = q.pop()
                if n.next:
                    q.append(n.next)
        elif p:
            n = p.pop()
            if n.next:
                p.append(n.next)
        else:
            n = q.pop()
            if n.next:
                q.append(n.next)
        if not head:
            head = tail = n
        else:
            tail.next = n
            tail = n
    return head


def test_merge_sorted_stack():
    first = Node(1, Node(4, Node(6, Node(10, Node(11)))))
    second = Node(2, Node(7, Node(11, Node(20))))
    print("first: \n%s" % first)
    print("second: \n%s" % second)
    head = merge_sorted_stack(first, second)
    print("merged: \n%s" % head)


def merge_all_sorted_lists(*args):
    """merge a variable number of sorted lists
    solution is O(n^2) since args decreases
    """
    if not args:
        raise Exception("bad input")
    # merge sort given lists by data
    args = list(args)
    head = []
    while args:
        head = merge_sorted_stack(head, args.pop())
    return head


def merge_sorted(left, right):
    head, current, temp = None, None, None
    while left and right:
        if left.val < right.val:
            child = left.next
            temp = left
            left = child
        else:
            child = right.next
            temp = right
            right = child

        if not head:
            head = temp
            current = head
        else:
            current.next = temp
            current = temp
    if left:
        if current:
            current.next = left
        else:
            head = left
    if right:
        if current:
            current.next = right
        else:
            head = right
    return head

def make_list():
    return [
        Node(2, Node(4, Node(6, Node(10, Node(11))))),
        Node(1, Node(7, Node(11, Node(20))))
    ]

def main():
    print("lists: \n", make_list())
    print("merge_sorted():       ", merge_sorted(*make_list()))
    print("merge_sorted_stack(): ", merge_sorted_stack(*make_list()))


if __name__ == "__main__":
    main()