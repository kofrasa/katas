#!/usr/bin/env python

def find_loop(head):
    """Check if there is a loop in the linked list
    """
    if not head:
        raise Exception("bad input")
    try:
        fast = head
        while head:
            # fast should jump twice ahead
            fast = fast.next.next
            head = head.next
            if head == fast:
                return True
    # use exception to prevent cumbersome error handling
    except:
        pass
    return False