#!/usr/bin/env python

from node import Node, gen_linkedlist

def reverse_with_stack(head):
    """reverse a linkedlist
    O(n) time, O(n) space using stack
    """

    # push nodes onto a stack
    # then pop while reassigning links
    # eventhough this an O(n) algorithm it runs relatively slower and uses O(n) space
    # reverse2() offers a better approach

    if not head:
        raise Exception("bad")

    # iterate and switch next and prev
    current = head
    L = []

    # head->next01->next02
    # push nodes on a stack
    # O(n)
    while current:
        L.append(current)
        current = current.next

    # next02->next01->head
    # pop nodes and update references
    # O(n)
    for i in range(len(L)):
        node = L.pop()
        node.next = None
        if not current:
            current = node
            head = current
        else:
            current.next = node
            current = node
    return head


def reverse2(head):
    # Reverses list in O(n) time with O(1) space
    current = None
    while head:
        next = head.next
        head.next = current
        current = head
        head = next
    head = current
    return head

def reverseK(root, k):
    """ Reverses first k nodes in O(n) time and O(1) space
    :return
    """
    if not root:
        return root
    head, current, i = root, None, 0
    while head and i < k:
        child = head.next
        head.next = current
        current = head
        head = child
        i += 1
    return current, head, i

def reverseKGroup(root, k):
    # handle empty
    if not root:
        return root
    head, current, i = reverseK(root, k)
    # if we did not reverse k elements, restore order of last set of nodes
    if i < k:
        head, current, i = reverseK(head, k)

    root = head
    prev = None
    # seek to last node
    while head:
        prev, head = head, head.next
    if prev:
        n = reverseKGroup(current, k)
        prev.next = n
    return root

def reverseKGroup2(root, k):
    # handle empty
    if not root:
        return root
    # count nodes
    head, n = root, 0
    while head:
        head = head.next
        n += 1

    # we need to rotate n sets
    n = int(n / k)

    head, root, last, current = root, None, None, None
    while n:
        head, current, i = reverseK(head, k)
        # set root if we don't have it
        if not root:
            root = head
        if not last:
            # if there is no last node, take the current head
            last = head
        else:
            while last and last.next:
                last = last.next
            last.next = head
        head = current
        n -= 1
    if current:
        while last and last.next:
            last = last.next
        last.next = current
    if not root:
        root = head
    return root

# 1->2->3->4->5
# 2->1->3

def main():
    head = gen_linkedlist(1, 2)
    print("before reverse:", head)
    # head = reverse(head)
    # print("before reverse2:", head)
    # head = reverse2(head)
    # print ("after reverse2:", head)
    print("resverseKGroup: \n",
        reverseKGroup2(head, 3))


if __name__ == '__main__':
    main()
