#!/usr/bin/env python

from node import Node

def is_palindrome(head):
    """Check if the linkedlist is a palindrome without extra space
    """
    # given a linked list A->B->C->B->A
    # compare head to tail chopping of each end until nodes are exhausted
    # this solution is O(n)
    if not head:
        raise Exception("bad input")
    while head:
        next = head.next
        if not next:
            break
        prev = next
        while next.next:
            prev = next
            next = next.next
        if next.val != head.val:
            return False
        head = head.next
        prev.next = None
    return True


def main():
    data = raw_input("enter string: ")
    head = Node(data[0].strip())
    next = head
    for s in data[1:]:
        next.next = Node(s)
        next = next.next
    print("List: %s" % head)
    print("Is Palindrome: %s" % is_palindrome(head))


if __name__ == '__main__':
    main()
