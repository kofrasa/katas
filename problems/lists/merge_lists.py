#!/usr/bin/env python

import random
from collections import deque
from .node import Node, gen_linkedlist

def merge_list(a, b):
    """Merge 2 linked lists into one
    """
    if not a:
        return b
    if not b:
        return a
    head = a
    tail = head
    a = a.next
    while a or b:
        if b:
            tail.next = b
            b = b.next
            tail = tail.next
        if a:
            tail.next = a
            a = a.next
            tail = tail.next
    return head


def merge_all_lists(*args):
    """merge multiple linked lists into one interposing nodes
    """
    Q = deque([])
    for node in args:
        if node:
            Q.append(node)
    head = Q.popleft()
    tail = head
    Q.append(head.next)
    while Q:
        n = Q.popleft()
        if not n:
            continue
        tail.next = n
        tail = n
        if n.next:
            Q.append(n.next)
        tail.next = None
    return head


def main():
    a = Node(1)
    t = a
    for i in range(10, 20):
        t.next = Node(i)
        t = t.next
    b = Node(5)
    t = b
    for i in range(30, 40, 2):
        t.next = Node(i)
        t = t.next
    print("list 1: %s" % a)
    print("list 2: %s" % b)
    h = merge_list(a, b)
    print("merged list: %s" % h)

    L = []
    print("merging multiple linkedlists:")
    for i in range(5):
        d = gen_linkedlist(random.randint(1, 5), random.randint(5, 10), -1)
        print(d)
        L.append(d)
    print("merged list: %s" % merge_all_lists(*L))


if __name__ == "__main__":
    main()