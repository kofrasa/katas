#!/usr/bin/env python

def find_join(A, B):  # solution 1
    """
    2 linked lists, join at some node, figure out which node that is

    e.g. given 2 linked lists:
      A -> B -> C -> D -> E -> F
      G -> H -> ↑
      find D
    """
    # use a hash to track the  nodes
    h = {}
    while A:
        h[A] = 1
        A = A.next
    while B:
        if h[B]:
            return h[B]
        h[B] = 1
        B = B.next
    return None

def main():
    pass

if __name__ == '__main__':
    main()
