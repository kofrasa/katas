#!/usr/bin/env python

"""
https://leetcode.com/discuss/interview-question/337515/Google-or-Onsite-or-Chunked-Palindrome

Normal palindrome is defined as a string that reads the same backwards as forwards, for example "abccba".
Chunked palindrome is defined as a string that you can split into chunks and it will form a palindrome.
For example, "volvo". You can split it into (vo)(l)(vo). Let A = "vo", B = "l", so the original string is ABA which is a palindrome.

Given a string str, find the maximum number of chunks we can split that string to get a chuncked palindrome.
"""

from .is_palindrome import is_palindrome
from .split_palindromes import split_palindromes

def chunked_palindrome_shortest(s):
    """
    Normal palindrome is defined as a string that reads the same backwards as forwards, for example "abccba".
    Chunked palindrome is defined as a string that you can split into chunks and it will form a palindrome.
    For example, "volvo". You can split it into (vo)(l)(vo). Let A = "vo", B = "l", so the original string is ABA which is a palindrome.

    Given a string str, find the maximum number of chunks we can split that string to get a chuncked palindrome.
    """
    # O(n) space and time complexity.
    # 1. Return 0 for empty input
    # 2. Return len(input) if input is palindrome
    # 3. Construct a stack from front and back characters to create sequence with longest palindromes at the top.
    # 4. Split longests palindromes and remaing characters into 2 halves
    # 5. IF chunks.length == 0, RETURN MIN(1, remaining.length) - all remaing characters count as 1.
    # 6. IF chunks.length == 1 AND remaining.length == 0 AND all characters repeating: RETURN chunks[0].length. This handles an edge case
    # 7. RETURN 2^chunks.length + MIN(1, remaining.length)

    stack = []
    i, j = 0, len(s)-1
    while i < j:
        stack.append(s[i])
        stack.append(s[j])
        i += 1
        j -= 1
    if i == j:
        stack.append(s[i])

    # split palindromes from top in O(n)
    chunks, remaining = split_palindromes(''.join(stack))

    if not chunks:
        # all remaining characters count as 1
        return min(1, len(remaining))
    elif not remaining and len(chunks) == 1 and all(c == chunks[0][0] for c in chunks[0]):
        # handle edge case for when the input is a repeating character
        return 1
    else:
        # chunks is at lest one here so no risk of 2^0
        return 2**len(chunks) + min(1, len(remaining))

def main():
    for k,n in {
        "": 0,
        "a": 1,
        "volvolvo":5,
        "volvol":2,
        "vovo":2,
        "volov": 5,
        "aaaaaa":1,
        "voabcvo":3,
        "valve":1,
        "voovlvolvoov": 5,
        "voovllvoov":4,
        "voovlaclacvoov": 4,
        "aaaaaabaaaaaa": 3}.items():
        print(k, "expected:", n, "actual:", chunked_palindrome_shortest(k))

if __name__ == '__main__':
    main()
