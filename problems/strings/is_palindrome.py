#!/usr/bin/env python

def is_palindrome(s):
    """Check if a string is a palindrome
    """
    i, j = 0, len(s)
    while i < j:
        if s[i] != s[j - i - 1]:
            return False
        i += 1
    return True

