#!/usr/bin/env python


def longest_palindrome_iter(s):

    if not s:
        return 0

    n = len(s)
    L = [[0] * len(s) for _ in range(n)]

    # fill diagonals
    for i in range(n):
        L[i][i] += 1

    max_length = 1

    # check for k length palindromes where k >= 2
    # k is the length of substring to consider
    for k in range(2, n+1):
        for i in range(n - k + 1):
            # Get the ending index of substring from starting index i and length k
            j = i + k - 1

            # checking for sub-string from ith index to jth index iff s[i+1] to s[j-1] is a palindrome
            if s[i] == s[j]:
                if k == 2:
                    L[i][j] = 2
                else:
                    L[i][j] = L[i+1][j-1] + 2
            else:
                L[i][j] = max(L[i][j-1], L[i+1][j])


    return L[0][n-1]

def longest_palindrome_recur(s):
    # Intuition:
    # Let L(0,n) be the longest palindrome sequence in s[0..n].
    # L(0,n) = L(1,n-1) + 2  => 2 characters for both ends match
    #       OR
    # L(0,n) = MAX( L(0,n-1), L(1,n) ) => maximum of L(i,j) adjusted to remove non-matching ends

    if not s:
        return 0

    def lps(seq, i, n, memo=None):
        if n == i:
            return 1
        if n < i:
            return 0

        if memo is None:
            memo = {}

        # key for hasmemo
        k = (i, n)
        if k in memo:
            return memo[k]

        if seq[i] == seq[n]:
            memo[k] = lps(seq, i+1, n-1, memo) + 2
        else:
            memo[k] = max(lps(seq, i+1, n, memo), lps(seq, i, n-1, memo))

        return memo[k]

    return lps(s, 0, len(s)-1)

def main():
    fixtures = {
        "": 0, "a": 1, "ab": 1,
        "aa":2, "aab":2, "abbc":2,
        "aba": 3, "aaa": 3, "qabaem": 3,
        "amavoov": 4, "amavoovlol": 4
    }
    for k,v in fixtures.items():
        print("input:", k, "expect:", v, "actual:", longest_palindrome_recur(k))
        print("input:", k, "expect:", v, "actual:", longest_palindrome_iter(k))

if __name__ == '__main__':
    main()
