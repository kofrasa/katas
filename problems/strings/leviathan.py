#!/usr/bin/env python

def lev_distance(a, b):
    if not a:
        return len(b)
    if not b:
        return len(a)
    if a[0] == b[0]:
        return lev_distance(a[1:], b[1:])
    else:
        return lev_distance(a[1:], b[1:]) + 1


if __name__ == "__main__":
    for a, b in [('cat', 'cow'), ('boy', 'girl'), ('mirage', 'disassociate')]:
        print("words: (%s, %s) -> %s" % (a, b, lev_distance(a, b)))