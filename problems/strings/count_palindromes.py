#!/usr/bin/env python

def count_palindromes(s):
    """Given a string find the total number of palindromes that can be created.
    Some characters in string are replaced with '?' and can be replaced with any character in [a-z]
    String contains only characters in [a-z] and '?'.
    """
    n = len(s)
    i = 0
    j = n - 1
    c = 0
    while i <= j:
        if s[i] == s[j]:
            if s[i] == '?':
                # we can vary both and create as many as [a-z]
                c += 26
        elif s[i] == '?' or s[j] == '?':
            # can only vary one to the other
            c += 1
        else:
            # string is not a palindrome
            c = 0
            break
        i += 1
        j -= 1
    return c

if __name__ == '__main__':
    for s, k in [("a??a", 26), ("ma?am", 26), ("r?c?c?r", 52), ("al?e?a", 2), ("djs??as", 0)]:
        print("s= %s. expect: %s. actual: %s" % (s, k, count_palindromes(s)))
