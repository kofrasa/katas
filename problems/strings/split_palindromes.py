#!/usr/bin/env python

from .is_palindrome import is_palindrome

def split_palindromes(s):
    """Split longest matching palindromes from beginning on input.
    This O(n) time and space
    """
    i, j = 0, len(s)
    stack = []
    while j - i > 1: # we need to check at least two characters
        p = s[i:j]
        if is_palindrome(p):
            stack.append(p)
            s = s[j:]
            j = len(s)
        else:
            j -= 1
    return stack, s