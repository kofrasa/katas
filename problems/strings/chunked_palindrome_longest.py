#!/usr/bin/env python

"""
https://leetcode.com/discuss/interview-question/337515/Google-or-Onsite-or-Chunked-Palindrome

Normal palindrome is defined as a string that reads the same backwards as forwards, for example "abccba".
Chunked palindrome is defined as a string that you can split into chunks and it will form a palindrome.
For example, "volvo". You can split it into (vo)(l)(vo). Let A = "vo", B = "l", so the original string is ABA which is a palindrome.

Given a string str, find the maximum number of chunks we can split that string to get a chuncked palindrome.
"""
from .split_palindromes import split_palindromes

def chunked_palindrome_longest(s):
    """
    Normal palindrome is defined as a string that reads the same backwards as forwards, for example "abccba".
    Chunked palindrome is defined as a string that you can split into chunks and it will form a palindrome.
    For example, "volvo". You can split it into (vo)(l)(vo). Let A = "vo", B = "l", so the original string is ABA which is a palindrome.

    Given a string str, find the maximum number of chunks we can split that string to get a chuncked palindrome.
    """
    # O(n) space and time complexity.

    stack = []
    i, j = 0, len(s)-1
    while i < j:
        stack.append(s[i])
        stack.append(s[j])
        i += 1
        j -= 1
    if i == j:
        stack.append(s[i])

    # split palindromes from top in O(n)
    chunks, remaining = split_palindromes(''.join(stack))

    if not chunks:
        # all remaining characters count as 1
        return min(1, len(remaining))
    else:
        total = 0
        for c in chunks:
            if c[0] == c[1]:
                total += len(c)
            else:
                total += 2
        return total + min(1, len(remaining))

def main():
    for k,n in {
        "": 0,
        "a": 1,
        "volvolvo":5,
        "volvol":2,
        "vovo":2,
        "volov": 5,
        "aaaaaa":6,
        "voabcvo":3,
        "valve":1,
        "voovlvolvoov": 11,
        "voovllvoov":10,
        "voovlaclacvoov": 10,
        "aaaaaabaaaaaa": 13}.items():
        print(k, "expected:", n, "actual:", chunked_palindrome_longest(k))

if __name__ == '__main__':
    main()
