#!/usr/env/bin python

"""
https://leetcode.com/problems/palindrome-partitioning/

Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

Example:

Input: "aab"
Output:
[
  ["aa","b"],
  ["a","a","b"]
]
"""

def partition(s):
    """
    :type s: str
    :rtype: List[List[str]]
    """
    # solution:
    # 1. tokenize palindromes in string
    # 2. create map of index to palindrome strings
    # 3. iterate each index jumping to next from string lenght to build sets
    # 4. when index not in set (we exhausted input), add set to result

    def is_palindrome(chars):
        i, n = 0, len(chars) - 1
        while i <= n:
            if chars[i] != chars[n]:
                return False
            n -= 1
            i += 1
        return True

    def permute(res,prev,i,hmap):
        if i not in hmap:
            res.append(prev)
        else:
            for sub in hmap[i]:
                permute(res, prev+[sub],len(sub)+i,hmap)
        return res

    # O(n^2) worst case when 's' -> 'aaaaaa' (all the same characters)
    H, size = {}, len(s)
    for i in range(size):
        H[i], j = [], size
        while i < j:
            t = "".join(s[i:j])
            if is_palindrome(t):
                H[i].append(t)
            j -= 1

    return permute([],[],0,H)


def main():
    fixture = {
        "aaaaa": [
            ["aaaaa"],
            ["aaaa","a"],
            ["aaa","aa"],
            ["aaa","a","a"],
            ["aa","aaa"],
            ["aa","aa","a"],
            ["aa","a","aa"],
            ["aa","a","a","a"],
            ["a","aaaa"],
            ["a","aaa","a"],
            ["a","aa","aa"],
            ["a","aa","a","a"],
            ["a","a","aaa"],
            ["a","a","aa","a"],
            ["a","a","a","aa"],
            ["a","a","a","a","a"]],
        "abbdsvoov": [
            ["a","bb","d","s","voov"],
            ["a","bb","d","s","v","oo","v"],
            ["a","bb","d","s","v","o","o","v"],
            ["a","b","b","d","s","voov"],
            ["a","b","b","d","s","v","oo","v"],
            ["a","b","b","d","s","v","o","o","v"]
        ],
    }

    for s in fixture.keys():
        r = partition(s)
        print("input:", s, "passed:", r == fixture[s])

if __name__ == '__main__':
    main()
