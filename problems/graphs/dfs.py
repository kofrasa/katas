#!/usr/bin/env python

def dfs(graph, v):
    """Traverse the graph using depth-first search
    O(n) time using stack and set
    :param v: the vertex to start exploring from
    """
    stack = [v]
    visited = []
    while stack:
        v = stack.pop()
        if v not in visited:
            visited.append(v)
            for u in graph[v]:
                stack.append(u)
    return visited


def main():
    g = {'a': ['b', 'c', 'e'], 'b': ['d', 'f', 'a'], 'c': ['g', 'a'], 'd': ['b'], 'e': ['f'], 'f': ['e'], 'g': ['c']}
    print("graph: %s" % g)
    print("dfs: %s" % dfs(g, 'a'))


if __name__ == '__main__':
    main()
