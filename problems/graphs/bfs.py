#!/usr/bin/env python


from collections import deque

def bfs(graph, v):
    """Traverse graph using breadth-first search
    O(n) time using queue
    :param graph:
    :param v:
    :return:
    """
    visited = []
    q = deque([v])
    while q:
        v = q.popleft()
        if v not in visited:
            visited.append(v)
            for u in graph[v]:
                q.append(u)
    return visited

def main():
    g = {'a': ['b', 'c', 'e'], 'b': ['d', 'f', 'a'], 'c': ['g', 'a'], 'd': ['b'], 'e': ['f'], 'f': ['e'], 'g': ['c']}
    print("graph: %s" % g)
    print("bfs: %s" % bfs(g, 'a'))


if __name__ == '__main__':
    main()
