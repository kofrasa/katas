# -*- coding: utf-8 -*-

class Graph(object):
    """A graph data structure using an adjacency list representation
    """

    def __init__(self, vertices):
        # vertices
        self.vertices = []
        for i in range(vertices):
            self.vertices.append(set([]))

    def add(self, v, w):
        """Add an edge between the given vertices
        """
        self._validate(v)
        self._validate(w)
        assert v != w
        self.vertices[v].add(w)
        self.vertices[w].add(v)

    def adj(self, v):
        """Returns the vertices adjacent to the given vertex
        """
        self._validate(v)
        return self.vertices[v]

    def _validate(self, v):
        assert 0 <= v <= len(self.vertices)