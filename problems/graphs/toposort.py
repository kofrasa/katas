#!/usr/bin/env python

from collections import deque

def toposort(g):
    nodes = list(g.keys())
    tmp = []
    seen = []
    res = deque([])

    def visit(n):
        if n in tmp:
            return False
        if n not in seen:
            tmp.append(n)
            for v in g[n]:
                if not visit(v):
                    return False
            seen.append(n)
            tmp.remove(n)
            res.appendleft(n)
        return True

    while nodes:
        if not visit(nodes.pop()):
            return []
    return res


def main():
    g = {7: [11, 8], 5: [11], 3: [8, 10], 11: [2, 9, 10], 2: [], 9: [], 8: [9], 10: []}
    print("graph: %s" % g)
    print("toposort: %s" % toposort(g))


if __name__ == '__main__':
    main()
