#!/usr/bin/env python

def product_k_consecutive(arr, k):
    """Find the product of k consecutive numbers in array
    """
    last = 1 # store last product
    result = []
    for i, n in enumerate(arr):
        acc = n*last
        if i > k:
            div = arr[i-k]
            if div != 0:
                acc /= div
        last = acc
        result.append(int(acc))
    return result

def main():
    k = 3
    nums = [1, 3, 3, 6, 5, 7, 0, -3]
    output = product_k_consecutive(nums, k)
    expected = [1, 3, 9, 54, 90, 210, 0, 0]
    print("actual:", output, "\nexpect:", expected, "\nequal:", output == expected, "\n")

if __name__ == '__main__':
    main()