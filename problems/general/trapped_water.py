#!/usr/bin/env python

def trap(height):
    "Find the amount of water trapped"
    i, j = 0, len(height)-1
    left_max = right_max = 0
    ans = 0
    while i < j:
        left_max = max(left_max,height[i])
        right_max = max(right_max,height[j])
        if left_max <= right_max:
            ans += (left_max-height[i])
            i += 1
        else:
            ans += (right_max-height[j])
            j -= 1
    return ans

def main():
    pass

if __name__ == "__main__":
    main()
