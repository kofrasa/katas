#!/usr/bin/env python

def fib(n):
    """Computes the fibonacci number for the given term
    :param n: the term of the fibonacci sequence
    """
    if not n:
        return 0

    # 0 1 1 2 3 5 8 13 ...
    previous = 0
    current = 1
    while n - 1:
        current, previous = (previous + current), current
        n -= 1
    return current

if __name__ == '__main__':
    for i in range(10):
        print("In:", i, "Out:", fib(i))
