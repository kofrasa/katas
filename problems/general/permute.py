#!/usr/bin/env python

def permute(xs):
    """Returns a generator for the permutations of elements in the sequence

    :param xs: a sequence of elements
    """
    if len(xs) == 1:
        yield xs
    else:
        for i in range(0, len(xs)):
            for p in permute(xs[0:i] + xs[i + 1:]):
                yield [xs[i]] + p