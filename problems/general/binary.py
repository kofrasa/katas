#!/usr/bin/env python

def int2bytea(num, width=4):
  """Convert an integer to a byte array
  :param num: the number
  :param width: the minimum number of bytes to use for representing the number
  """
  arr = bytearray()
  count = 0
  while num:
    arr.append(0xff & num)
    num >>= 8
    count += 1

  arr.extend([0] * (width - count))
  arr.reverse()
  return arr


def int2bytes(num, width=4):
  """Convert an integer to a byte string
  :param num: the number
  :param width: the minimum number of bytes to use for representing the number
  """
  return ''.join([chr(s) for s in int2bytea(num, width)])