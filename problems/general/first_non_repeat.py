#!/usr/bin/env python


def non_repeating():
    H = dict()
    head = None
    tail = None
    for c in sys.stdin:
        c = c.strip()
        if c in H:
            p = H[c]
            if p.data[0] == 1:
                head, tail = lists.remove_node(head, tail, p)
            p.data[0] += 1
            H[c] = p
        else:
            p = lists.Dnode([1, c])
            head, tail = lists.append_node(head, tail, p)
            H[c] = p
    if head:
        print("First non-repeating character is %s" % head.data[1])
    else:
        print("No non-repeating character")