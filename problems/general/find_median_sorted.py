#!/usr/bin/env python

# There are two sorted arrays nums1 and nums2 of size m and n respectively.
# Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
# You may assume nums1 and nums2 cannot be both empty.
# A typical solution would create a new array from both
def find_median_sorted(nums1, nums2):
    """
    :type nums1: List[int]
    :type nums2: List[int]
    :rtype: float
    """
    if len(nums1) > len(nums2):
        nums1, nums2 = nums2, nums1

    s1, s2 = len(nums1), len(nums2)

    size = s1 + s2
    rem = size % 2
    mid = int(size/2) + 1

    # deque
    dq, di = [None,None], 0
    i, j = 0, 0
    while (i+j) < mid:
        if i < s1 and j < s2:
            if nums1[i] < nums2[j]:
                dq[di&1] = nums1[i]
                i += 1
            else:
                dq[di&1] = nums2[j]
                j += 1
        elif i < s1:
            dq[di&1] = nums1[i]
            i += 1
        elif j < s2:
            dq[di&1] = nums2[j]
            j += 1
        di = (di+1) & 1

    # single median value
    if rem:
        if dq[1] is not None:
            return max(dq)
        return dq[0]

    # compute
    return sum(dq)/2.0


def find_median_sorted2(nums1, nums2):
    # merge sorted arrays
    l = sorted(nums1 + nums2)
    mid = int(len(l)/2)
    if len(l) & 1 == 0:
        return sum(l[mid-1:mid+1])/2.0
    return l[mid]


def main():
    A = [1,2,5,13]
    B = [3,4,11,13,14]
    print("find_median_sorted:", find_median_sorted(A, B))
    print("find_median_sorted2:", find_median_sorted2(A, B))


if __name__ == '__main__':
    main()
