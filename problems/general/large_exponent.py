#!/usr/bin/env python

def large_exponent(n, exp):
    """Computing the square of very large numbers using exponentiation by squaring
    """
    # x^n is such that:
    # if x is even:
    # (x * x)^(n/2)
    # else:
    # x * [(x * x) ^ ((n-1)/2)]
    #
    # O(log2n) squarings and O(log2n) multiplications
    # complexity: O(logn)
    if exp < 0:
        return large_exponent(1 / n, -exp)
    if exp == 0:
        return 1
    if exp == 1:
        return n
    if n % 2 == 0:
        return large_exponent(n * n, exp / 2)
    else:
        return n * large_exponent(n * n, (exp - 1) / 2)


def main():
    for a, b in [(2, 5), (3, 8), (7, 18)]:
        print("%s^%s: %s" % (a, b, pow(a, b)))
        print("large_exp: %s" % large_exponent(a, b))

if __name__ == '__main__':
    main()
