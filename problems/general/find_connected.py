#!/usr/bin/env python

def find_connected_recurse(M):
    pass


def find_connected(M):
    """Given a NxM array find the number and indexes of connected slots
    """
    visited = {}
    def search(i, j):
        item = M[i][j]

        if (i,j) not in visited:
            visited[(i,j)] = []

        for k in [-1, 1]:
            k += j
            if (i, k) not in visited and k >= 0 and k < len(M[i]) and M[i][k] == item:
                visited[(i,j)].append((i, k))
                search(i, k)
        for k in [-1, 1]:
            k += i
            if (k, j) not in visited and k >= 0 and k < len(M) and M[k][j] == item:
                visited[(i,j)].append((k, j))
                search(k, j)

    # build graph
    for i in range(len(M)):
        for j in range(len(M[i])):
            if (i,j) not in visited:
                search(i, j)

    # read graph
    for i in range(len(M)):
        for j in range(len(M[i])):
            if (i,j) in visited:
                stack = visited[(i,j)]
                res = []
                while stack:
                    p = stack.pop()
                    res.append(p)
                    if p in visited:
                        stack.extend(visited[p])
                        del visited[p]
                visited[(i,j)] = res
                if not res:
                    del visited[(i,j)]
    return visited

def main():
    M = [[1,0,0,1,0],
        [0,1,0,0,1],
        [1,1,1,0,1],
        [0,1,0,1,0]]

    print("In:", M, "\nOut:", find_connected(M))

if __name__ == '__main__':
    main()
