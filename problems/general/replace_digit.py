#!/usr/bin/env python

def replace_digit(n, old=0, new=5):
    # ex. 32094 -> 32594
    # n % 10 => last digit
    # if last digit is zero displace to position with 5
    rem = 0
    acc = 0
    factor = 1
    while n:
        rem = n % 10  # get last digit
        n //= 10  # drop last digit
        if rem == old:
            rem = new
        acc += (factor * rem)
        factor *= 10
    return acc


def main():
    n, old, new = map(int, raw_input("Enter <N> <old_digit> <new_digit>: ").split())
    print("%s --> %s" % (n, replace_digit(n, old, new)))

if __name__ == '__main__':
    main()
