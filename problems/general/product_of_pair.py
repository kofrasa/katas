#!/usr/bin/env python

def product_of_pair(array, product):
    """Find the pair of numbers whose product is given. O(n)
    """
    result = []
    if product == 0:
        if any(x == 0 for x in array):
            for x in array:
                if x != 0:
                    result.append((0, x))
            result.append((0,0))
    else:
        hset = set(array)
        for n in array:
            if n == 0:
                continue
            m = int(product / n)
            if m in hset:
                result.append((n, m))
                hset.discard(m)
                hset.discard(n)

    return result

def main():
    fixtures = [
        [ [], 20, [] ],
        [ [2,0,4,5,6], 0, [(0,2), (0,4), (0,5), (0,6)] ],
        [ [2,0,4,5,6], 10, [(2,5)] ],
        [ [2,3,4,5,6], 12, [(2,6), (3,4)] ],
        [ [1,2,3,4,5], 4, [(1,4), (2,2)] ],
        [ [-3, 0, -1, 2], -6, [(-3,2)] ]
    ]

    for array, product, expected in fixtures:
        print("array:", array, "\nproduct:", product,
            "\nexpect:", expected, "\nactual:", product_of_pair(array, product), "\n")

if __name__ == "__main__":
    main()