#!/usr/bin/env python

"""
https://leetcode.com/problems/merge-intervals/

Given a collection of intervals, merge all overlapping intervals.

Example 1:

Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
NOTE: input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.
"""

def merge_intervals(intervals):
    """
    :type intervals: List[List[int]]
    :rtype: List[List[int]]
    """
    # intervals are overlaping when
    #   1. boundaries touch
    #   2. boundaries intersect
    h = {}
    for x, y in intervals:
        if x not in h:
            h[x] = y
        else: # X boundaries touch
            h[x] = max(h[x], y)

    current = None
    for x in sorted(h.keys()):
        if current is None or x > h[current]:
            current = x
        else:
            h[current] = max(h[current], h[x])
            del h[x]

    return h.items()