#!/usr/bin/env python

def next_greater_element(array):
    """Find the next greater element of each item
    Print -1 for elements with no next greater element
    """
    if array is None:
        raise Exception("bad input")
    items = []
    result = []
    for i in array:
        while items and items[-1] < i:
            result.append((items.pop(), i))
        items.append(i)
    while items:
        result.append((items.pop(), -1))
    return result


def main():
    fixtures = [
        [ [], [] ],
        [ [0], [(0,-1)] ],
        [ [13, 7, 6, 12], [(6,12), (7,12), (12,-1), (13,-1)] ],
        [ [1, 2, 3, 4], [(1,2), (2,3), (3,4)] ]
    ]
    for array, expected in fixtures:
        print("input:", array, "\nexpect:", expected, "\nactual:", next_greater_element(array), "\n")

if __name__ == '__main__':
    main()
