#!/usr/bin/env python

def consecutive_int(n):
    """Find the next biggest integer in consecutive order following the given integer
    """
    seq = "123456789"
    while str(n) not in seq:
        n += 1
    return n


def main():
    for n in [118, 127, 987]:
        print("%s: %s" % (n, consecutive_int(n)))

if __name__ == '__main__':
    main()
