#!/usr/bin/env python

def max_matrix_row(M):
    """Find the row with the maximum number of ones
    Each row contains 0s and 1s and is sorted
    """
    # loop through each row
    # use binary search to find first index of 1
    # compute number of 1s in row and store index if max
    # O(nlogn)
    import bisect

    if not M:
        raise Exception("bad input")
    cols = len(M[0])
    rows = len(M)
    index = -1
    cur = 0
    for i in range(rows):  # O(n)
        j = bisect.bisect_left(M[i], 1)  # O(logn)
        if j != -1:
            s = cols - j
            if s > cur:
                cur = s
                index = i
        if s == cols:
            break
    return index


def max_matrix_row2(M):
    """Use a more efficient approach O(m+n)
    Store last column index and continue from there on the next row
    """
    if not M:
        raise Exception("bad input")
    cols = len(M[0])
    rows = len(M)
    index = 0
    j = cols - 1
    for i in range(rows):
        while M[i][j] == 1 and j >= 0:
            j -= 1
            index = i
        if j < 0:
            break
    return index


def main():
    M = [[0, 1, 1, 1], [0, 0, 1, 1], [1, 1, 1, 1], [0, 0, 0, 0]]
    print("matrix: %s" % M)
    print("max row index: expect: 2, actual: %s" % max_matrix_row2(M))

if __name__ == '__main__':
    main()
