#!/usr/bin/env python

def rotate_matrix(A):
    """Rotate a matrix 90 degrees. Can do this inplace if matrix is a square
    this happens in O(n^2)
    """
    # algorithm
    # reverse the rows
    # swap A[i,j] and A[j,i]
    if not A:
        raise Exception("Bad input")

    assert isinstance(A[0], list)

    rows = len(A)
    cols = len(A[0])

    B = A[:]
    # reverse the rows in O(n^2)
    for row in B:
        row.reverse()
    # result  will be a col x row matrix after rotation
    M = []
    for i in range(cols):
        M.append([0] * rows)
    # O(n^2)
    for i in range(rows):
        for j in range(cols):
            M[j][i] = B[i][j]
    return M


def main():
    A = [list("abc"), list("def")]
    print("Matrix: %s" % A)
    print("Rotated: %s" % rotate_matrix(A))

if __name__ == '__main__':
    main()
