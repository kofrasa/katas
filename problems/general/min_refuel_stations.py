#!/usr/bin/env python

"""
https://leetcode.com/problems/minimum-number-of-refueling-stops/

A car travels from a starting position to a destination which is target miles east of the starting position.

Along the way, there are gas stations.  Each station[i] represents a gas station that is station[i][0] miles east of the starting position, and has station[i][1] liters of gas.

The car starts with an infinite tank of gas, which initially has tank liters of fuel in it.  It uses 1 liter of gas per 1 mile that it drives.

When the car reaches a gas station, it may stop and refuel, transferring all the gas from the station into the car.

What is the least number of refueling stops the car must make in order to reach its destination?  If it cannot reach the destination, return -1.

Note that if the car reaches a gas station with 0 fuel left, the car can still refuel there.  If the car reaches the destination with 0 fuel left, it is still considered to have arrived.
"""
import heapq

class Solution(object):
    def minRefuelStops(self, target, tank, stations):
        """
        :type target: int
        :type tank: int
        :type stations: List[List[int]]
        :rtype: int
        """
        pq = []
        ans = prev = 0
        mileage = tank # total mileage covered
        stations.append((target, float('inf')))
        for location, capacity in stations:
            tank -= location - prev
            while pq and tank < 0:
                if mileage >= target:
                    return ans
                fuel = -heapq.heappop(pq)
                tank += fuel
                mileage += fuel
                ans += 1
            if tank < 0:
                return -1
            heapq.heappush(pq, -capacity)
            prev = location
        return ans

def main():
    fixtures = [
        [1000, 83, [[47,220],[65,1],[98,113],[126,196],[186,218],[320,205],[686,317],[707,325],[754,104],[781,105]]],
        [1000, 83, [[15,457],[156,194],[160,156],[230,314],[390,159],[621,20],[642,123],[679,301],[739,229],[751,174]]],
        [100, 25, [[25,25],[50,25],[75,25]]],
        [100, 1, [[10,100]]],
        [1000, 299, [[13,21],[26,115],[100,47],[225,99],[299,141],[444,198],[608,190],[636,157],[647,255],[841,123]]],
        [1, 10, [[10,60],[20,30],[30,30],[60,40]]],
        [1, 1, []],
        [1000, 299, [[14,123],[145,203],[344,26],[357,68],[390,35],[478,135],[685,108],[823,186],[934,217],[959,80]]],
        [444, 299, [[13,21],[26,115],[100,47],[225,99],[299,141],[439,115],[608,190],[636,157],[647,255],[841,123]]]
    ]
    solution = Solution()
    for target, tank, stations in fixtures:
        print("target:", target, "tank:", tank,
        "\nstations:", stations, "\nans:", solution.minRefuelStops(target, tank, stations), "\n")

if __name__ == '__main__':
    main()
