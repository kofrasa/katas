#!/usr/bin/env python

def max_product(N, k=3):
    """Find the k numbers in N whose product is maximum
    """
    if not N or len(N) < k:
        raise Exception("bad input")
    N.sort(reverse=True)
    return N[:k]