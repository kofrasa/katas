#!/usr/bin/env python

def matching_parentheses(n):
    res = []
    def solve(s, left, right):
        if right == n:
            res.append(s)
        else:
            if left > right:
                solve(s + ")", left, right + 1)
            if left < n:
                solve(s + "(", left + 1, right)

    solve("", 0, 0)
    return res


def main():
    for i in range(4):
        print("n:", i, "\nparen:", matching_parentheses(i), "\n")

if __name__ == '__main__':
    main()
