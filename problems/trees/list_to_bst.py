#!/usr/bin/env python

from .treenode import TreeNode

def sortedlist_to_bst(head):
    """Convert a sorted linked list to BST
    """
    # use binary search method and and create a subtree for left and right at
    # each division
    xs = []
    while head:
        xs.append(TreeNode(head.val))
        head = head.next

    def get_tree(lo, hi):
        mid = lo + (hi - lo) / 2
        if lo < hi:
            n = xs[mid]
            n.left = get_tree(lo, mid)
            n.right = get_tree(mid + 1, hi)
            return n
        else:
            return None

    return get_tree(0, len(xs))


def main():
    from problems.lists import Node

    head = Node(0)
    temp = head
    for i in range(1, 15):
        temp.next = Node(i)
        temp = temp.next
    root = sortedlist_to_bst(head)
    print(root)

if __name__ == '__main__':
    main()
