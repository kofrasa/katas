#!/usr/bin/env python

class Heap(object):
    def __init__(self):
        self.values = [0]

    def __repr__(self):
        return str(self.values[1:])

    def size(self):
        return len(self.values) - 1

    def add(self, value):
        self.values.append(value)
        self.swim(self.size())

    def update(self, index, new_value):
        old_value = self.values[index]
        self.values[index] = new_value
        if old_value > new_value:
            self.swim(index)
        else:
            self.sink(index)

    def swim(self, k):
        while k > 1:
            parent = self.values[k / 2]
            if parent > self.values[k]:
                a, b = self.values[k / 2], self.values[k]
                self.values[k], self.values[k / 2] = a, b
                k /= 2
            else:
                break

    def sink(self, k):
        while 2 * k <= self.size():
            i = 2 * k
            if i < self.size() and self.values[i] > self.values[i + 1]:
                i += 1
            if self.values[k] > self.values[i]:
                self.values[k], self.values[i] = self.values[i], self.values[k]
                k = i
            else:
                break


import random

def test_minheap():
    h = Heap()
    for i in range(10):
        h.add(random.randint(5, 100))
    print(h)

    h.update(10, 2)
    print(h)


if __name__ == "__main__":
    test_minheap()