#!/usr/bin/env python

from .treenode import gen_tree

def print_distance(root, k):
    """Prints all nodes at k distance from given node
    """
    if not root:
        return
    if not k:
        print(root.val)
    else:
        print_distance(root.left, k - 1)
        print_distance(root.right, k - 1)


def main():
    root = gen_tree(10)
    print("distance: %s" % 5)
    print_distance(root, 5)
    print(root)

if __name__ == '__main__':
    main()
