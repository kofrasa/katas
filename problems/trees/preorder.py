#!/usr/bin/env python

from .treenode import TreeNode

def preorder_recur(root):
    """ root -> left -> right
    """
    if root:
        print(root.val)
        preorder_recur(root.left)
        preorder_recur(root.right)


def preorder(root):
    S = []
    while root or S:
        if root:
            print(root.val)
            if root.right:
                S.append(root.right)
            root = root.left
        else:
            root = S.pop()

def main():
    root = TreeNode('f')
    for i in ['b', 'g', 'a', 'd', 'c', 'e', 'i', 'h']:
        root.add(TreeNode(i.upper()))

    print("Tree: ", root)

    print("preorder-iterative:")
    preorder(root)

    print("preorder-recur:")
    preorder_recur(root)


if __name__ == '__main__':
    main()
