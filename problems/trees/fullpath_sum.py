#!/usr/bin/env python

from .treenode import TreeNode

def fullpath_sum(root, k):
    """
    Given a Binary tree, full_path_sum is sum of all nodes from root to leaf in a path.
    Given a min_sum value, delete nodes if path has full_path_sum less than min_sum. Delete all such nodes.
    For example,
    Given min_sum = 8
            1
        2       3
     4    5   6   7

    So we delete 4
    """
    if not root:
        raise Exception("bad root")

    def is_leaf(n):
        return n.left is n.right is None

    def remove_leaf(p, n):
        if p:
            if p.left is n:
                p.left = None
            if p.right is n:
                p.right = None

    if is_leaf(root) and root.val > k:
        return None

    def traverse(n, parent, acc):
        if not n:
            return
        acc += n.val
        if acc < k:
            if is_leaf(n):
                remove_leaf(parent, n)
            else:
                traverse(n.left, n, acc)
                traverse(n.right, n, acc)

    traverse(root, None, 0)


def main():
    root = TreeNode(1,
                    TreeNode(2, TreeNode(4), TreeNode(5)),
                    TreeNode(3, TreeNode(6), TreeNode(7)))

    print("Before: using k=8.", root)
    fullpath_sum(root, 8)
    print("After: must remove 4.", root)


if __name__ == '__main__':
    main()
