#!/usr/bin/env python

from collections import deque
from .treenode import TreeNode

def fix_bst(root):
    """A BST has 2 nodes swapped. fix the BST
    """

    # traverse tree inorder for values and store in an array
    # sort the array
    # traverse the tree inorder putting back the correct values
    def inorder_pop(n):
        if not n:
            return
        inorder_pop(n.left)
        N.append(n.val)
        inorder_pop(n.right)

    def inorder_swap(n):
        if not n:
            return
        inorder_swap(n.left)
        n.val = Q.popleft()
        inorder_swap(n.right)

    N = []
    inorder_pop(root)
    N.sort()
    Q = deque(N)
    inorder_swap(root)


def main():
    root = TreeNode(10, TreeNode(5, TreeNode(2), TreeNode(20)), TreeNode(8))
    print("Before: \n%s" % root)
    fix_bst(root)
    print("After: \n%s" % root)


if __name__ == '__main__':
    main()
