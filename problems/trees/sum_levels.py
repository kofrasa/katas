#!/usr/bin/env python

from collections import deque
from .treenode import TreeNode

def sum_levels(root):
    """Sum odd and even levels of tree
    """
    if not root:
        raise Exception("bad root")

    def acc(node, k):
        g, h = (0, 0)
        if node.left:
            g, h = map(sum, zip(acc(node.left, k + 1), [g, h]))
        if node.right:
            g, h = map(sum, zip(acc(node.right, k + 1), [g, h]))
        if k % 2:
            g += node.val
        else:
            h += node.val
        return g, h

    odd, even = acc(root, 1)
    return odd - even


def sum_levels_iter(root):
    """Sum odd and even levels of tree
    """
    if not root:
        raise Exception("bad root")
    odd, even = (0, 0)
    q = deque([])
    q.append((root, 1))
    while q:
        n, v = q.popleft()
        if v % 2:
            odd += n.val
        else:
            even += n.val
        if n.left:
            q.append((n.left, v + 1))
        if n.right:
            q.append((n.right, v + 1))
    return odd - even


def test_sum_levels():
    root = TreeNode(5,
                    TreeNode(2, TreeNode(1), TreeNode(4, TreeNode(3))),
                    TreeNode(6, None, TreeNode(8, TreeNode(7), TreeNode(9))))
    print("Expected: -9. Actual: %s" % sum_levels(root))
    print("Expected: -9. Actual: %s" % sum_levels_iter(root))