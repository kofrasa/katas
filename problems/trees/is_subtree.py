#!/usr/bin/env python

from .treenode import TreeNode

def is_subtree(s, t):
    if s == t:
        return True
    if not s:
        return False
    if not t:
        return False

    if s.val == t.val:
        left = is_subtree(s.left, t.left)
        right = is_subtree(s.right, t.right)
        return left and right
    else:
        left = is_subtree(s.left, t)
        right = is_subtree(s.right, t)
        return left or right

def main():
    # s = TreeNode(1, TreeNode(2), TreeNode(3, TreeNode(4), TreeNode(5)))
    # t = TreeNode(3, TreeNode(4), TreeNode(5))
    s = TreeNode(26,
                 TreeNode(10, TreeNode(4, None, TreeNode(30)), TreeNode(6)),
                 TreeNode(3, None, TreeNode(3)))

    t = TreeNode(10, TreeNode(4, None, TreeNode(30)), TreeNode(6))
    print("is subtree: %s" % is_subtree(s, t))

if __name__ == "__main__":
    main()