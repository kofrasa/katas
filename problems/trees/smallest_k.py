#!/usr/bin/env python

from .treenode import TreeNode

def find_smallest_k(root, k):
    """Find kth smallest element in BST
    Use inorder traversal
    """
    S = []
    i = 0
    while root or S:
        if root:
            S.append(root)
            root = root.left
        else:
            root = S.pop()
            i += 1
            if i == k:
                return root.val
            root = root.right
    return -1


def main():
    root = TreeNode(20,
                    TreeNode(8,
                             TreeNode(4),
                             TreeNode(12, TreeNode(10), TreeNode(14))),
                    TreeNode(22))
    k = 3
    print("tree:", root)
    print("Element at k=%s: expected: 10, actual: %s" % (k, find_smallest_k(root, k)))


if __name__ == '__main__':
    main()
