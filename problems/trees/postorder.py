#!/usr/bin/env python

from .treenode import TreeNode

def postorder_recur(root):
    """ left -> right -> root
    """
    if root:
        postorder_recur(root.left)
        postorder_recur(root.right)
        print(root.val)

def main():
    root = TreeNode('f')
    for i in ['b', 'g', 'a', 'd', 'c', 'e', 'i', 'h']:
        root.add(TreeNode(i.upper()))

    print("Tree: ", root)

    print("postorder:")
    postorder_recur(root)

if __name__ == '__main__':
    main()
