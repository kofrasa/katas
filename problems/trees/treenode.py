import random

class TreeNode(object):
    def __init__(self, val=None, left=None, right=None):
        self.val = val
        self.right = right
        self.left = left

    def __repr__(self):
        return self.display()

    def display(self, depth=0):
        s = []
        prefix = ""
        if depth:
            prefix = ("  |" * (depth - 1)) + "  "
        s.append(prefix + "+-" + str(self.val) + "\n")
        if self.left:
            s.append(self.left.display(depth + 1))
        if self.right:
            s.append(self.right.display(depth + 1))
        return "".join(s)

    def pretty(self, depth=0):
        prefix = ""
        if depth:
            prefix = "  " * (depth)
        if self.left or self.right:
            prefix += "\-"
        else:
            prefix += "|-"
        print(prefix + str(self.val))
        if self.left:
            self.left.pretty(depth + 1)
        if self.right:
            self.right.pretty(depth + 1)

    def add(self, node):
        if not node:
            return
        if node.val <= self.val:
            if self.left:
                self.left.add(node)
            else:
                self.left = node
        else:
            if self.right:
                self.right.add(node)
            else:
                self.right = node

def gen_tree(k):
    root = TreeNode(random.randint(1, 100))
    for i in range(2 * k):
        root.add(TreeNode(random.randint(1, 100)))
    return root