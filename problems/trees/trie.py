#!/usr/bin/env python

class Node:
    def __init__(self, value):
        self.value = value
        # we assume string keys in ascii set
        self.nodes = None

    def __repr__(self):
        nodes = self.nodes
        if nodes:
            '\n'.join(map(str, self.nodes))
        return "%s:\n\t%s" % (self.value, nodes)


class Trie:
    def __init__(self):
        self.node = Node(None)

    def put(self, key, value):
        if not key:
            return key
        root = self.node
        for c in key:
            i, children = ord(c), root.nodes
            if not children:
                children = [0]*256 # assumes ascii character set
                root.nodes = children
            if not children[i]:
                children[i] = Node(c)
            root = children[i]
        root.value = value

    def get(self, key):
        if not key:
            return None
        root = self.node
        res = []
        for c in key:
            i, children = ord(c), root.nodes
            if not children:
                return None
            root = children[i]
        return root.value

    def delete(self, key):
        if not key:
            return None
        root = self.node
        prev = root
        for c in key:
            i, children = ord(c), root.nodes
            if not children:
                return
            prev = root
            root = children[i]
        #TODO: swim up to delete empty parents
        root.value = None
        # delete nodes to save space if empty
        if root.nodes is None and not any(prev.nodes):
            prev.nodes = None

    def __repr__(self):
        return "%s" % (self.node)


def main():
    t = Trie()
    fixtures = [["ama", "person"], ["anaconda", "snake"], ["apple", "fruit"]]
    for k,v in fixtures:
        t.put(k,v)
    for k,v in fixtures:
        print("key:", k, "expect:", v, "actual:", t.get(k))

if __name__ == '__main__':
    main()
