#!/usr/bin/env python

from collections import deque
from .treenode import TreeNode

def check_bst(root):
    """Checks if a tree is a binary search tree in O(n)
    """

    if not root:
        raise Exception("invalid node object")

    L = deque([], 2)  # max queue length is 2

    # traverse tree in-order to get items
    # if item count is 2 check if they are in correct order

    # O(n)
    def traverse(node):

        if not node:
            return True

        # terminate if nodes
        if len(L) == 2 and L[0] > L[1]:
            return False

        if traverse(node.left):
            L.append(node.val)
            return traverse(node.right)

        return False

    return traverse(root)


def main():
    root = TreeNode(23)
    for i in range(10):
        root.add(TreeNode(random.randint(1, 100)))

    print("IS_BST: %s \n%s" % (check_bst(root), root))

    # create bad tree
    root = TreeNode(12, TreeNode(8), TreeNode(11))
    for i in range(10):
        root.add(TreeNode(random.randint(1, 100)))

    print("IS_NOT_BST: %s \n%s" % (not check_bst(root), root))


if __name__ == '__main__':
    main()
