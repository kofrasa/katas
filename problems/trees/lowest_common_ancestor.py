#!/usr/bin/env python

from .treenode import TreeNode

def lowest_common_ancestor(root, A, B):
    """Compute the lowest common ancestor of values on a tree
    :param root TreeNode
    :param A int
    :param B int
    """
    while root:
        if root.val < A and root.val < B:
            root = root.right
        elif root.val > A and root.val > B:
            root = root.left
        else:
            break
    return root


def main():
    root = TreeNode(20,
                    TreeNode(8,
                             TreeNode(4),
                             TreeNode(12, TreeNode(10), TreeNode(14))),
                    TreeNode(22))
    print("Expect 12: Actual: %s" % lowest_common_ancestor(root, 10, 14))
    print("Expect 8: Actual: %s" % lowest_common_ancestor(root, 8, 14))


if __name__ == '__main__':
    main()
