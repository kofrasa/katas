#!/usr/bin/env python

from .treenode import TreeNode

def inorder_recur(root):
    """ left -> root -> right (will print a BST in order)
    """
    if root:
        inorder_recur(root.left)
        print(root.val)
        inorder_recur(root.right)


def inorder(root):
    """Traverse tree by left -> root -> right at each node.
    For a BST nodes will be listed in order
    """
    S = []
    while root or S:
        if root:
            S.append(root)
            root = root.left
        else:
            root = S.pop()
            print(root.val)
            root = root.right

def main():
    root = TreeNode('f')
    for i in ['b', 'g', 'a', 'd', 'c', 'e', 'i', 'h']:
        root.add(TreeNode(i.upper()))

    print("Tree: ", root)
    print("inorder:")
    inorder_recur(root)


if __name__ == '__main__':
    main()
