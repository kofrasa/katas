#!/usr/bin/env python

from collections import deque
from .treenode import TreeNode

def levelorder(root):
    """traverse nodes level by level.
    """
    # print node and enque children at the next level
    # repeat until queue is empty
    Q = deque([root])
    while Q:
        root = Q.popleft()
        print(root.val)
        if root.left:
            Q.append(root.left)
        if root.right:
            Q.append(root.right)

def main():
    root = TreeNode('f')
    for i in ['b', 'g', 'a', 'd', 'c', 'e', 'i', 'h']:
        root.add(TreeNode(i.upper()))

    print("Tree: ", root)
    print("level order:")
    levelorder(root)

if __name__ == '__main__':
    main()
