#!/usr/bin/env python

from .treenode import TreeNode

def spiralorder(root):
    """ root -> left -> right -> right -> left
    """
    if not root:
        return
    S = [root]
    T = []
    while S or T:
        while S:
            n = S.pop()
            print(n.val)
            if n.right:
                T.append(n.right)
            if n.left:
                T.append(n.left)
        while T:
            n = T.pop()
            print(n.val)
            if n.left:
                S.append(n.left)
            if n.right:
                S.append(n.right)

def main():
    root = TreeNode(1,
                    TreeNode(2, TreeNode(7), TreeNode(6)),
                    TreeNode(3, TreeNode(5), TreeNode(4)))
    print("Tree:", root)
    print("spiral order:")
    spiralorder(root)


if __name__ == '__main__':
    main()
