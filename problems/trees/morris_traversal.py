#!/usr/bin/env python

from .treenode import TreeNode

def morris_traversal(root):
    """An iterative inorder traversal without using an extra memory
    """
    # iteratively build an ordered linkedlist while traversing
    while root:
        if root.left:
            l = root.left
            r = root.left
            while r and r.right:
                r = r.right
            r.right = root
            root.left = None
            root = l
        else:
            print(root.val)
            root = root.right

def main():
    root = TreeNode('F')
    for i in ['b', 'g', 'a', 'd', 'c', 'e']:
        root.add(TreeNode(i.upper()))

    print("Tree: \n", root)

    print("morris traversal - iterative inorder without extra space:")
    morris_traversal(root)

if __name__ == '__main__':
    main()
