#!/usr/bin/env python

from collections import deque


def generate_sets(dimensions):
    """
    Generates the minimum number of combinations that use each member of
    each dimension at least once.

    Complexity: O(nm)
        n - size of largest dimension
        m - total number of dimensions
    """
    dimensions = [
        deque(d) for d in sorted(
            dimensions, key=lambda x: len(x), reverse=True)
    ]
    M = []  # matrix of results
    total = len(dimensions)
    head = dimensions[0]  # largest dimension set

    def cycle(q):
        e = q.popleft()
        q.append(e)

    while head:
        T = []  # current combination
        for i in range(total):
            D = dimensions[i]
            T.append(D[0])
            cycle(D)
        M.append(T)
        head.pop()

    return M


def main():
    dimensions = [["diamond", "cocoa"],
                  ["america", "africa", "europe", "asia"],
                  ["red", "blue", "white"], ["air", "land", "sea"]]
    expected = [["america", "red", "air", "diamond"],
                ["africa", "blue", "land", "cocoa"],
                ["europe", "white", "sea", "diamond"],
                ["asia", "red", "air", "cocoa"]]
    result = generate_sets(dimensions=dimensions)
    print("Pass: %s" % (expected == result))
    print("Expected: %s" % expected)
    print("Result: %s" % result)


if __name__ == '__main__':
    main()
